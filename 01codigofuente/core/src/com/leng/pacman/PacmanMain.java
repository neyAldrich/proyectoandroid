package com.leng.pacman;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.leng.pacman.game.Assets;
import com.leng.pacman.game.WorldController;
import com.leng.pacman.game.WorldRenderer;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.leng.pacman.game.Assets;
import com.leng.pacman.screens.MenuScreen;
import com.leng.pacman.util.AudioManager;

public class PacmanMain extends /*ApplicationAdapter*/ Game {

	private static final String TAG = PacmanMain.class.getName();
	private boolean paused; 	//< Se usa para manejar la p�rdida de foco en Android

    public WorldController worldController;
    public WorldRenderer worldRenderer;

    @Override
	public void create () {

        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        // cargar assets
        Assets.instance.init(new AssetManager());
        //AudioManager.instance.play(Assets.instance.sounds.pacman_intermission);
        AudioManager.instance.play(Assets.instance.music.song03);
        // Start game at menu screen
        setScreen(new MenuScreen(this));

        /*
        // Inicializar controlador y renderizador
        worldController = new WorldController();
        worldRenderer = new WorldRenderer(worldController);

        // El juego est� activo al arrancar
        paused = false;*/
    }

    /*
	@Override
	public void render () {
        // No se actualiza el mundo si est� pausado
        if (!paused){
            // Actualizar el mundo de acuerdo al tiempo transcurrido desde el ultimo frame
            worldController.update(Gdx.graphics.getDeltaTime());
        }
        // Establecer el color de limpieza de pantalla en NEGRO
		Gdx.gl.glClearColor(0, 0, 0, 1);

        // Limpia la pantalla
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Renderizar el mundo
        worldRenderer.render();
	}

    @Override
    public void resize(int width, int height){
        worldRenderer.resize(width, height);
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        Assets.instance.init(new AssetManager());
        paused = false;
    }

    @Override
    public void dispose() {
        worldRenderer.dispose();
        Assets.instance.dispose();
    }*/
}
