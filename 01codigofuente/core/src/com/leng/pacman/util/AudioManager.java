package com.leng.pacman.util;

/**
 * Created by pc on 04/08/2015.
 */

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;


public class AudioManager {
    public static final AudioManager instance = new AudioManager();

    /*public static final GamePreferences instance = new GamePreferences();

    public float volSound;
    public float volMusic;
    private Preferences prefs;
    prefs = Gdx.app.getPreferences(Constants.PREFERENCES);
    sound = prefs.getBoolean("sound", true);
    music = prefs.getBoolean("music", true);
    volSound = MathUtils.clamp(prefs.getFloat("volSound", 0.5f), 0.0f, 1.0f);
    volMusic = MathUtils.clamp(prefs.getFloat("volMusic", 0.5f), 0.0f, 1.0f);*/

    private Music playingMusic;
    // singleton: prevent instantiation from other classes
    private AudioManager () { }
    public void play (Sound sound) {
        play(sound, 1);
    }
    public void play (Sound sound, float volume) {
        play(sound, volume, 1);
    }
    public void play (Sound sound, float volume, float pitch) {
        play(sound, volume, pitch, 0);
    }
    public void play (Sound sound, float volume, float pitch,
                      float pan) {

        sound.play(0.5f,pitch, pan);
        /*if (!GamePreferences.instance.sound) return;
        sound.play(GamePreferences.instance.volSound * volume,
                pitch, pan);*/
    }

    public void play (Music music) {
        stopMusic();
        playingMusic = music;
        music.setLooping(true);
        music.setVolume(0.5f);
        music.play();
        /*if (GamePreferences.instance.music) {
            music.setLooping(true);
            music.setVolume(GamePreferences.instance.volMusic);
            music.play();
        }*/
    }
    public void stopMusic () {
        if (playingMusic != null) playingMusic.stop();
    }

}
