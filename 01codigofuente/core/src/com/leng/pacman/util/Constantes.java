package com.leng.pacman.util;

/**
 * Created by Neycker on 19/07/2015.
 */
public class Constantes {
    public static final float VIEWPORT_WIDTH = 45.0f;
    public static final float VIEWPORT_HEIGHT = 31.0f;


    public static final float VIEWPORT_GUI_WIDTH = 800.0f;
    public static final float VIEWPORT_GUI_HEIGHT = 480.0f;

    public static final String TEXTURE_ATLAS_OBJECTS = "images/pacman.atlas";
    public static final String TEXTURE_ATLAS_MENU = "images/pacman-ui.atlas";

    public static final String SKIN_PACMAN_UI = "images/pacmanui";

    // Duracion del poder en segundos
    public static final float ITEM_COCO_POWERUP_DURATION = 10;

    // Direcciones hacia los niveles
    public static final String NIVEL_01 = "levels/nivel1.txt";
    public static final String NIVEL_02 = "levels/nivel2.txt";
    public static final String NIVEL_03 = "levels/nivel3.txt";

    // VIDAS POR DEFECTO
    public static final int VIDAS = 3;

    // VELOCIDAD M�XIMA DE PACMAN
    public static final float PACMAN_VELOCITY = 5.0f;

}