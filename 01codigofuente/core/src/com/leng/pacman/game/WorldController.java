package com.leng.pacman.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.leng.pacman.game.objects.*;
import com.leng.pacman.util.Constantes;

import com.leng.pacman.game.objects.Coco;

import java.util.HashMap;
import java.util.Random;

import com.badlogic.gdx.Game;
import com.leng.pacman.screens.MenuScreen;
import com.leng.pacman.util.AudioManager;


/**
 * Created by Neycker on 19/07/2015.
 */
public class WorldController extends InputAdapter {
    public static final String TAG = WorldController.class.getName();

    public Nivel nivel;
    public int vidas;
    public int puntuacion;
    private Random randGen;
    private float tiempoJuego;
    public boolean debugMode;

    private Array<String> pathNiveles;
    private int nivelActual;

    private float timeLeftGameOverDelay;

    private Game game;

    private void backToMenu () {
        // switch to menu screen
        game.setScreen(new MenuScreen(game));
        AudioManager.instance.play(Assets.instance.music.song03);
    }

    public WorldController(Game game){
        this.game = game;
        init();
    }

    public void init(){
        AudioManager.instance.play(Assets.instance.sounds.pacman_beginning);
        debugMode = false;
        randGen = new Random();
        Gdx.input.setInputProcessor(this);
        vidas = Constantes.VIDAS;
        tiempoJuego = 0;
        puntuacion = 0;

        nivelActual = 0; // Nivel 1 es 0 para pasarlo como indice en el arreglo de pathNiveles.

        pathNiveles = new Array<String>();
        pathNiveles.add(Constantes.NIVEL_01);
        pathNiveles.add(Constantes.NIVEL_02);
        pathNiveles.add(Constantes.NIVEL_03);

        initLevel();
    }

    public void initLevel(){
        nivel = new Nivel();
        nivel.cargarNivel(pathNiveles.get(nivelActual));
        nivel.frutaActual = nivelActual;
    }

    public void update(float deltaTime) {
        handleDebugInput(deltaTime);

        if (isGameOver()) {
            timeLeftGameOverDelay -= deltaTime;
            if (timeLeftGameOverDelay < 0) backToMenu();
            if (timeLeftGameOverDelay < 0) init();
        } else {
            handleInputGame(deltaTime);
        }


        nivel.update(deltaTime);
        tiempoJuego += deltaTime;
        testCollisions();
    }


    public boolean isGameOver () { return vidas <= 0; }

    // INPUT -----------

    @Override
    public boolean keyUp(int keycode){
        // Pause
        if (keycode ==Keys.P){
            // IMPLEMENTAR
        }
        // Back to Menu
        else if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
            backToMenu();
        }
        return false;
    }

    private void handleDebugInput(float deltaTime){
        // Input Escritorio
        if (Gdx.app.getType() == Application.ApplicationType.Desktop && debugMode) {
            if (Gdx.input.isKeyPressed(Keys.M)) {
                nivel.pacman.maxVelocity = Math.min(Constantes.PACMAN_VELOCITY * 3, nivel.pacman.maxVelocity + 1);
            }else if (Gdx.input.isKeyPressed(Keys.N)) {
                nivel.pacman.maxVelocity = Math.max(0, nivel.pacman.maxVelocity - 1);
            }else if(Gdx.input.isKeyPressed(Keys.B)){
                nivel.pacman.maxVelocity = Constantes.PACMAN_VELOCITY;
            } //Cambio de Nivel
            else if (Gdx.input.isKeyJustPressed(Keys.K)){
                if (nivelActual < pathNiveles.size - 1) {
                    nivel = null;
                    nivelActual++;

                    initLevel();
                }
            }else if (Gdx.input.isKeyJustPressed(Keys.J)) {
                if (nivelActual > 0) {
                    nivel = null;
                    nivelActual--;

                    initLevel();
                }
            }else if (Gdx.input.isKeyJustPressed(Keys.H)) {
                vidas++;
            }
        }
    }

    private void handleInputGame (float deltaTime){
        // Input Escritorio
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            if (Gdx.input.isKeyPressed(Keys.W)) {
                nivel.pacman.setMovDirection(Agente.DIRECTION.UP);
            }else if(Gdx.input.isKeyPressed(Keys.S)){
                nivel.pacman.setMovDirection(Agente.DIRECTION.DOWN);
            }else if(Gdx.input.isKeyPressed(Keys.A)){
                nivel.pacman.setMovDirection(Agente.DIRECTION.LEFT);
            }else if(Gdx.input.isKeyPressed(Keys.D)){
                nivel.pacman.setMovDirection(Agente.DIRECTION.RIGHT);
            }

            if (Gdx.input.isKeyJustPressed(Keys.U)){
                debugMode = !debugMode;

                if(!debugMode) nivel.pacman.maxVelocity = Constantes.PACMAN_VELOCITY;
            }
        } else if( Gdx.app.getType() == Application.ApplicationType.Android){

                int deltaX = Gdx.input.getDeltaX();
                int deltaY = Gdx.input.getDeltaY();

                if (Math.abs(deltaX) > Math.abs(deltaY)){
                    if (deltaX > 0){
                        nivel.pacman.setMovDirection(Agente.DIRECTION.RIGHT);
                    }else if (deltaX < 0){
                        nivel.pacman.setMovDirection(Agente.DIRECTION.LEFT);
                    }
                }else{
                    if (deltaY > 0){
                        nivel.pacman.setMovDirection(Agente.DIRECTION.DOWN);
                    }else if(deltaY < 0){
                        nivel.pacman.setMovDirection(Agente.DIRECTION.UP);
                    }
                }

        }
    }


    // COLISIONES ----------

    // Rectangulos para detectar colisiones
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();

    private void onCollisionAgenteWithMuro(Agente agente, Muro muro){
        Vector2 distanceVector;
        float distMin;

        float radioAgente = agente.dimension.x / 2.0f;
        float radioMuro = muro.dimension.x / 2.0f;

        Vector2 posCentroAgente = agente.getCenterPos();
                //new Vector2(agente.position).add(new Vector2(radioAgente,radioAgente));
        Vector2 posCentroMuro = muro.getCenterPos();
                //new Vector2(muro.position).add(new Vector2(radioMuro, radioMuro));

        distMin = radioAgente + radioMuro;

        distanceVector = posCentroAgente.sub(posCentroMuro);

        float xDepth = distMin - Math.abs(distanceVector.x);
        float yDepth = distMin - Math.abs(distanceVector.y);

        if (xDepth > 0 || yDepth > 0 ){
            if (Math.max(xDepth, 0.0f) < Math.max(yDepth, 0.0f)){
                agente.position.x += (distanceVector.x > 0.0f) ? xDepth : -xDepth;
            } else {
                agente.position.y += (distanceVector.y > 0.0f) ? yDepth : -yDepth;
            }
        }
    }

    private void onCollisionPacmanWithCoco(Coco coco){
        coco.collected = true;
        AudioManager.instance.play(Assets.instance.sounds.new_chomp);

        puntuacion+=coco.getScore();
        Gdx.app.log(TAG, "Coco comido");

        if (coco.getTipo() == Coco.TIPO_COCO.SUPER){
            AudioManager.instance.play(Assets.instance.music.song01);
            this.nivel.pacman.setPacmanPowerup(true);
        }

        if (nivel.cocosComidos == nivel.numCocos ) { // Paso de nivel o ganar
            Gdx.app.debug(TAG, "Nivel completado.");

            if (nivelActual < pathNiveles.size -1) {
                nivel = null;
                nivelActual++;
                initLevel();
            }else{
                // JUEGO GANADO
                Gdx.app.debug(TAG, "Ha ganado el juego.");
            }
        }
    }

    private void onCollisionPacmanWithFantasma(Fantasma fantasma){
        if (vidas > 0){
            if (!nivel.pacman.hasPacmanPowerup()){
                nivel.pacman.position.set(nivel.pacman.startPosition);
                vidas -= 1;

                // SOUND PACMAN MUERTO

                nivel.encarcelarTodosFantasmas(randGen);
            }else{
                // 4 segundos
                fantasma.encarcelar((randGen.nextFloat()*4.0f) + 1.0f);

                puntuacion += fantasma.getScore();

                // SOUND FANTASMA COMIDO
            }

            Gdx.app.debug(TAG, "PACMAN VIDAS: " + vidas );
        }else{
            // GAME OVER
            backToMenu();
        }
    }

    private void onCollisionPacmanWithFruta(Fruta fruta){
        fruta.collected = true;
        puntuacion+=fruta.getScore();
        Gdx.app.debug(TAG, "Fruta comida: " +nivel.fruta.tipo.toString());

    }

    private void testCollisions(){

        // COLISIONES PACMAN <-> OTROS
        setRectFromGameObject(r1, nivel.pacman);

        // Probar colision Pacmam <-> Muros
        for (Muro muro : nivel.muros){
            setRectFromGameObject(r2, muro);

            if(!r1.overlaps(r2)) continue;

            onCollisionAgenteWithMuro(nivel.pacman, muro);
        }

        // Probar colision Pacman <-> Cocos
        for (int i = 0; i < nivel.cocos.size; i++) {

            Coco coco = nivel.cocos.get(i);

            setRectFromCoco(r2, coco);

            if(!r1.overlaps(r2)) continue;

            nivel.cocosComidos++;
            onCollisionPacmanWithCoco(coco);
            coco = null;
            nivel.cocos.removeIndex(i);
            Gdx.app.debug(TAG, "Cocos restantes: " + (nivel.numCocos - nivel.cocosComidos));
        }

        // Probar colision Pacman <-> fruta
        if (nivel.fruta != null){
            setRectFromGameObject(r2, nivel.fruta);

            if(r1.overlaps(r2)){
                onCollisionPacmanWithFruta(nivel.fruta);
                nivel.fruta = null;
            }
        }

        // Probar colision Pacman <-> Fantasmas
        for(Fantasma fantasma : nivel.fantasmas.values()){
            setRectFromGameObject(r2, fantasma);

            if(!r1.overlaps(r2)) continue;

            onCollisionPacmanWithFantasma(fantasma);

            Gdx.app.debug(TAG, "Colision Pacman - Fantasma");
        }

        // COLISIONES FANTASMAS <-> OTROS

        for(Fantasma fantasma : nivel.fantasmas.values()){
            setRectFromGameObject(r1, fantasma);

            for (Muro muro : nivel.muros){
                setRectFromGameObject(r2, muro);

                if(!r1.overlaps(r2)) continue;

                onCollisionAgenteWithMuro(fantasma, muro);
            }
        }
    }

    // AUXILIARES
    private void setRectFromGameObject(Rectangle r, AbstractGameObject object){
            r.set(object.position.x , object.position.y, object.bounds.width, object.bounds.height);
    }

    private void setRectFromCoco(Rectangle r, Coco coco){
        float ancho = coco.bounds.width;
        float alto = coco.bounds.height;

        r.set(coco.position.x + (ancho / 4.0f) , coco.position.y +(alto / 4.0f),
                coco.bounds.width / 4.0f, coco.bounds.height / 4.0f);
    }

    public String getTiempoFormateado(){
        String str = "";

        int minutos = 0;
        int segundos = 0;

        int tiempoSegundos = (int)tiempoJuego;

        if (tiempoSegundos > 0){
            minutos = tiempoSegundos / 60;
            segundos = tiempoSegundos % 60;

            str = "" + minutos + " : ";

            if (segundos < 10){
                str = str + "0" + segundos;
            }else{
                str = str + segundos;
            }
        }else{
            str = "0 : 00";
        }

        return str;
    }
}
