package com.leng.pacman.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.leng.pacman.util.Constantes;

/**
 * Created by Neycker on 19/07/2015.
 */
public class WorldRenderer implements Disposable {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private WorldController worldController;
    private OrthographicCamera cameraHUD;

    private GlyphLayout layout;

    public WorldRenderer(WorldController worldController){
        this.worldController = worldController;
        init();
    }

    public void init(){
        layout = new GlyphLayout();
        batch = new SpriteBatch();
        camera = new OrthographicCamera(Constantes.VIEWPORT_WIDTH, Constantes.VIEWPORT_HEIGHT);
        camera.position.set(Constantes.VIEWPORT_WIDTH / 2, Constantes.VIEWPORT_HEIGHT / 2, 0);
        camera.update();

        cameraHUD = new OrthographicCamera(Constantes.VIEWPORT_WIDTH, Constantes.VIEWPORT_HEIGHT);
        cameraHUD.position.set(0, 0, 0);
        cameraHUD.setToOrtho(true); // flip y-axis
        cameraHUD.update();
    }

    public void render(){
        renderWorld(batch);
        renderHUD(batch);
    }
    
    public void renderWorld(SpriteBatch batch){
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        worldController.nivel.render(batch);
        batch.end();
    }

    public void resize(int width, int height){
        //camera.viewportWidth =  Constantes.VIEWPORT_WIDTH /height * width;
        camera.update();

        /*cameraGUI.viewportHeight = Constantes.VIEWPORT_GUI_HEIGHT;
        cameraGUI.viewportWidth = (Constantes.VIEWPORT_GUI_HEIGHT / (float)height) * (float)width;
        cameraGUI.position.set(cameraGUI.viewportWidth / 2, cameraGUI.viewportHeight / 2, 0);*/
        cameraHUD.update();
    }

    @Override
    public void dispose(){

    }

    private void renderHUDScoreTime(SpriteBatch batch){

        layout.setText(Assets.instance.fonts.defaultNormal, "Tiempo Jugado:");

        float x = cameraHUD.viewportWidth - layout.width;
        float y = 75;

        Assets.instance.fonts.defaultNormal.draw(batch, "Puntuacion: " , x , y);
        y += layout.height + 15;

        Assets.instance.fonts.defaultNormal.draw(batch, "" + worldController.puntuacion, x , y, layout.width,
                Align.left, false);


        y += layout.height + 30;

        Assets.instance.fonts.defaultNormal.draw(batch, "Tiempo Jugado: ", x , y, layout.width,
                Align.left, false);

        y += layout.height + 15;

        Assets.instance.fonts.defaultNormal.draw(batch,worldController.getTiempoFormateado(), x , y, layout.width,
                Align.left, false);

        if (worldController.debugMode){
            y += layout.height + 75;

            Assets.instance.fonts.defaultNormal.setColor(Color.GREEN);

            Assets.instance.fonts.defaultNormal.draw(batch, "DEBUG: ON", x, y, layout.width,
                    Align.left, false);

            Assets.instance.fonts.defaultNormal.setColor(Color.WHITE);

        }
    }

    private void renderHUDExtraLive(SpriteBatch batch){
        float x = cameraHUD.viewportWidth - 40 - worldController.vidas * 50;
        float y = -15;
        for (int i = 0; i < worldController.vidas; i++) {
            if (worldController.vidas <= i) batch.setColor(0.5f, 0.5f, 0.5f, 0.5f);
            batch.draw(Assets.instance.pacman.pacman1, x + i*50, y, 50.0f, 50.0f, 120.0f, 100.0f, 0.35f, -0.35f, 0);
            batch.setColor(1, 1, 1, 1);
        }
    }

    private void renderHUD(SpriteBatch batch){
        batch.setProjectionMatrix(cameraHUD.combined);
        batch.begin();

        renderHUDExtraLive(batch);
        renderHUDScoreTime(batch);

        batch.end();
    }

}

