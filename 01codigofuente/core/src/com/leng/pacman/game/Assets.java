package com.leng.pacman.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Disposable;
import com.leng.pacman.util.Constantes;
import com.badlogic.gdx.graphics.Texture.TextureFilter;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by Neycker on 30/07/2015.
 */
public class Assets implements Disposable, AssetErrorListener {

    public static final String TAG = Assets.class.getName();
    public static final Assets instance = new Assets();
    private AssetManager assetManager;


    public AssetCoco coco;
    public AssetPacman pacman;
    public AssetFantasma fantasma;
    public AssetFrutas frutas;
    public AssetDecoracion decoracion;

    public AssetFonts fonts;
    public AssetSounds sounds;
    public AssetMusic music;

    //Singleton
    private Assets(){}

    /***
     * Creado por Neycker Aguayo. 29/07/2015
     * Inicializa el gestor de Recursos. Se cargan todas las texturas incluidas en un atlas
     * Se les habilita el filtro interlineado. Se muestran mensajes de debug por cada asset que se
     * va cargando. En caso de falla se ejecuta error.
     */
    public void init(AssetManager assetManager){
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);
        assetManager.load(Constantes.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);

        assetManager.load("sound/new_chomp.wav", Sound.class);
        assetManager.load("sound/new_siren.wav", Sound.class);
        assetManager.load("sound/Pacman_Introduction_Music-KP-826387403.wav", Sound.class);
        /*assetManager.load("sound/pacman_chomp.wav", Sound.class);
        assetManager.load("sound/pacman_death.wav", Sound.class);
        assetManager.load("sound/pacman_eatfruit.wav", Sound.class);
        assetManager.load("sound/pacman_eatghost.wav", Sound.class);
        assetManager.load("sound/pacman_extrapac.wav", Sound.class);
        assetManager.load("sound/pacman_intermission.wav", Sound.class);*/


        assetManager.load("sound/pacman_siren.mp3", Music.class);
        assetManager.load("sound/chomp.mp3", Music.class);
        assetManager.load("sound/e03ea5_Pacman_Intermission_Sound_Effect.mp3", Music.class);

        assetManager.finishLoading();

        Gdx.app.debug(TAG, "# de assets cargados: " + assetManager.getAssetNames().size);
        for (String a: assetManager.getAssetNames()){
            Gdx.app.debug(TAG, "asset: " + a);
        }

        TextureAtlas atlas = assetManager.get(Constantes.TEXTURE_ATLAS_OBJECTS);

        // Habilitando filtrado de textura
        for (Texture t : atlas.getTextures()){
            t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        }

        // Creando recursos de objetos del juego
        coco = new AssetCoco(atlas);
        pacman = new AssetPacman(atlas);
        fantasma = new AssetFantasma(atlas);
        frutas = new AssetFrutas(atlas);
        decoracion = new AssetDecoracion(atlas);

        fonts = new AssetFonts();
        sounds = new AssetSounds(assetManager);
        music = new AssetMusic(assetManager);
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "No se pudo cargar el asset '" + asset.fileName + "'", (Exception)throwable);
    }

    public void error(String filename, Class type, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '" + filename + "'", (Exception)throwable);
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose() {
        assetManager.dispose();
        fonts.defaultSmall.dispose();
        fonts.defaultNormal.dispose();
        fonts.defaultBig.dispose();
    }

    public class AssetCoco{
        public final AtlasRegion coco;
        public final AtlasRegion superCoco;

        public AssetCoco(TextureAtlas atlas){
            coco = atlas.findRegion("coco");
            superCoco = atlas.findRegion("superCoco");
        }
    }

    public class AssetPacman{
        // Imagen 1 de la animacion
        public final AtlasRegion pacman1;

        // Imagen 2 de la animacion
        public final AtlasRegion pacman2;

        public AssetPacman(TextureAtlas atlas){
            pacman1 = atlas.findRegion("pacman1");
            pacman2 = atlas.findRegion("pacman2");
        }
    }

    public class AssetFantasma{
        public final AtlasRegion rojo;
        public final AtlasRegion celeste;
        public final AtlasRegion amarillo;
        public final AtlasRegion rosa;

        public final AtlasRegion vulnerable;

        public AssetFantasma(TextureAtlas atlas){
            rojo = atlas.findRegion("fantasmaRojo");
            celeste = atlas.findRegion("fantasmaCeleste");
            amarillo = atlas.findRegion("fantasmaAmarillo");
            rosa = atlas.findRegion("fantasmaRosa");
            vulnerable = atlas.findRegion("fantasmaV1");
        }
    }

    public class AssetDecoracion{
        public final AtlasRegion muro_glass;
        public final AtlasRegion muro_light_bricks;
        public final AtlasRegion muro_red_bricks;
        public final AtlasRegion muro_simple;
        public final AtlasRegion puerta_prision;

        public AssetDecoracion(TextureAtlas atlas){
            muro_glass = atlas.findRegion("glass");
            muro_light_bricks = atlas.findRegion("light_bricks");
            muro_red_bricks = atlas.findRegion("red_bricks");
            muro_simple = atlas.findRegion("Rectangulo");
            puerta_prision = atlas.findRegion("puertaPrision");

        }
    }

    public class AssetFrutas{
        public final AtlasRegion cereza;
        public final AtlasRegion fresa;

        public AssetFrutas(TextureAtlas atlas){
            cereza = atlas.findRegion("cereza");
            fresa = atlas.findRegion("fresa");
        }
    }

    public class AssetFonts{
        public final BitmapFont defaultSmall;
        public final BitmapFont defaultNormal;
        public final BitmapFont defaultBig;

        public AssetFonts(){
            // Crear tres fuentes usando la fuente de 15 px Arial de LibGdx.
            defaultSmall = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
            defaultNormal = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
            defaultBig = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
            // Se define el tama�o de las fuentes.
            defaultSmall.getData().setScale(1.0f);
            defaultNormal.getData().setScale(2.0f);
            defaultBig.getData().setScale(4.0f);
            // Se habilita el filtro interlineado para suavizar los bordes.
            defaultSmall.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
            defaultNormal.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
            defaultBig.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

            // Color Blanco
            defaultSmall.setColor(Color.WHITE);
            defaultNormal.setColor(Color.WHITE);
            defaultBig.setColor(Color.WHITE);

            // defaultBig.draw()
        }
    }

    public class AssetSounds {

        public final Sound new_chomp;
        public final Sound new_siren;

        public final Sound pacman_beginning;
        /*public final Sound pacman_chomp;

        public final Sound pacman_death;
        public final Sound pacman_eatfruit;
        public final Sound pacman_eatghost;
        public final Sound pacman_extrapac;
        public final Sound pacman_intermission;
*/

        public AssetSounds (AssetManager am) {
            new_chomp = am.get("sound/new_chomp.wav", Sound.class);
            new_siren = am.get("sound/new_siren.wav", Sound.class);

            pacman_beginning = am.get("sound/Pacman_Introduction_Music-KP-826387403.wav", Sound.class);
            /*pacman_chomp = am.get("sound/pacman_chomp.wav", Sound.class);
            pacman_death = am.get("sound/pacman_death.wav", Sound.class);
            pacman_eatfruit = am.get("sound/pacman_eatfruit.wav", Sound.class);
            pacman_eatghost = am.get("sound/pacman_eatghost.wav", Sound.class);
            pacman_extrapac = am.get("sound/pacman_extrapac.wav", Sound.class);
            pacman_intermission = am.get("sound/pacman_intermission.wav", Sound.class);*/
        }
    }
    public class AssetMusic {
        public final Music song01;
        public final Music song02;
        public final Music song03;

        public AssetMusic (AssetManager am) {
            song01 = am.get("sound/pacman_siren.mp3", Music.class);
            song02 = am.get("sound/chomp.mp3", Music.class);
            song03 = am.get("sound/e03ea5_Pacman_Intermission_Sound_Effect.mp3", Music.class);
        }
    }
}
