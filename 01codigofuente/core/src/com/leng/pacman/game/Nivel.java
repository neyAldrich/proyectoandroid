package com.leng.pacman.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.leng.pacman.game.objects.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.FileHandler;

/**
 * Created by Neycker on 30/07/2015.
 * Parser para leer un nivel desde archivo .txt
 */
public class Nivel {

    public static final String TAG = Nivel.class.getName();
    private static Array<Fruta.TIPO_FRUTA> ordenFrutas = null;

    public int frutaActual = 0;

    public Array<String> nivelData;
    public int numCocos;
    public int cocosComidos;

    public Vector2 posicionSalida;

    // Atajos
    public Vector2 atajo1;
    public Vector2 atajo2;

    // Objetos
    public Fruta fruta;
    private Vector2 posFruta;
    public Array<Coco> cocos;
    public Array<Muro> muros;

    // Agentes
    public HashMap<Fantasma.TIPO_FANTASMA, Fantasma> fantasmas;
    public Pacman pacman;

    public Nivel(){
        init();
    }

    public void init() {
        this.nivelData = new Array<String>();
        this.numCocos = 0;
        this.cocosComidos = 0;

        this.posFruta = new Vector2();
        this.posicionSalida = new Vector2();
        this.atajo1 = new Vector2(0.0f, 0.0f);
        this.atajo2 = new Vector2(0.0f, 0.0f);
        this.fruta = null;
        this.muros = new Array<Muro>();
        this.fantasmas = new HashMap<Fantasma.TIPO_FANTASMA, Fantasma>();
        this.cocos = new Array<Coco>();
        this.pacman = null;

        if (ordenFrutas == null){
            ordenFrutas = ordenarFrutas();
        }
    }

    public void cargarNivel(String path){
        String line = null;
        Character baldosa = '!';

        try{
            FileHandle fr = Gdx.files.internal(path);

            BufferedReader br = new BufferedReader(fr.reader());

            //Eliminando la cabecera
            for (int i = 0; i < 5; i++) {
                br.readLine();
            }

            while ((line = br.readLine()) != null){
                nivelData.add(line);
            }

            for (int j = nivelData.size -1 ; j >= 0; j--) {
                for (int x = 0; x < nivelData.get(j).length(); x++) {

                    int y = nivelData.size -1 -j;

                    baldosa = nivelData.get(j).charAt(x);
                    Muro tempMuro;
                    Coco tempCoco;
                    Fantasma tempFantasma;

                    switch(baldosa){
                        // Muros del nivel
                        case 'M':   //< Muro Simple: SE UTILIZARA LIGHT_BRICKS DE TODAS FORMAS
                            tempMuro = new Muro(Muro.TIPO_MURO.LIGHT_BRICKS);
                            tempMuro.position.set(x,y);
                            this.muros.add(tempMuro);
                            break;
                        case 'X':
                            tempMuro = new Muro(Muro.TIPO_MURO.GLASS);
                            tempMuro.position.set(x, y);
                            this.muros.add(tempMuro);
                            break;
                        case 'Z':
                            tempMuro = new Muro(Muro.TIPO_MURO.RED_BRICKS);
                            tempMuro.position.set(x, y);
                            this.muros.add(tempMuro);
                            break;
                        case 'V':
                            tempMuro = new Muro(Muro.TIPO_MURO.LIGHT_BRICKS);
                            tempMuro.position.set(x, y);
                            this.muros.add(tempMuro);
                            break;

                        // Puerta de prision
                        case 'D':
                            tempMuro = new Muro(Muro.TIPO_MURO.PUERTA_PRISION);
                            tempMuro.position.set(x, y);

                            this.muros.add(tempMuro);
                            break;
                        case 'd':
                            tempMuro = new Muro(Muro.TIPO_MURO.PUERTA_PRISION);
                            tempMuro.position.set(x, y);
                            tempMuro.rotation = 90;

                            this.muros.add(tempMuro);
                            break;
                        case 'P':
                            tempMuro = new Muro(Muro.TIPO_MURO.SIMPLE);
                            tempMuro.position.set(x, y);
                            tempMuro.setTint(Color.YELLOW);

                            this.muros.add(tempMuro);
                            break;
                        case 'E':
                            posicionSalida.set(x, y);
                            break;

                        // Cocos
                        case '.':
                            tempCoco = new Coco(Coco.TIPO_COCO.NORMAL);
                            tempCoco.position.set(x, y);

                            this.cocos.add(tempCoco);
                            this.numCocos += 1;
                            break;
                        case 'S':
                            tempCoco = new Coco(Coco.TIPO_COCO.SUPER);
                            tempCoco.position.set(x, y);

                            this.cocos.add(tempCoco);
                            this.numCocos += 1;
                            break;

                        // Atajos
                        case '1':
                            this.atajo1.set(x, y);
                            break;
                        case '2':
                            this.atajo2.set(x, y);
                            break;
                        case '<':
                            tempMuro = new Muro(Muro.TIPO_MURO.SIMPLE);
                            tempMuro.position.set(x, y);
                            tempMuro.setTint(Color.BLACK);
                            this.muros.add(tempMuro);
                            break;

                        // Fruta
                        case 'f':
                            posFruta.set(x, y);
                            break;

                        // Fantasmas
                        case 'F': //FANTASMA ROJO
                            tempFantasma = new Fantasma(Fantasma.TIPO_FANTASMA.ROJO);
                            tempFantasma.position.set(x, y);
                            tempFantasma.startPosition.set(tempFantasma.position);
                            this.fantasmas.put(Fantasma.TIPO_FANTASMA.ROJO, tempFantasma);
                            break;
                        case 'R': //FANTASMA ROSADO
                            tempFantasma = new Fantasma(Fantasma.TIPO_FANTASMA.ROSA);
                            tempFantasma.position.set(x, y);
                            tempFantasma.startPosition.set(tempFantasma.position);
                            this.fantasmas.put(Fantasma.TIPO_FANTASMA.ROSA, tempFantasma);
                            break;
                        case 'C': //FANTASMA CELESTE
                            tempFantasma = new Fantasma(Fantasma.TIPO_FANTASMA.CELESTE);
                            tempFantasma.position.set(x, y);
                            tempFantasma.startPosition.set(tempFantasma.position);
                            this.fantasmas.put(Fantasma.TIPO_FANTASMA.CELESTE, tempFantasma);
                            break;
                        case 'A': //FANTAMSA AMARILLO
                            tempFantasma = new Fantasma(Fantasma.TIPO_FANTASMA.AMARILLO);
                            tempFantasma.position.set(x, y);
                            tempFantasma.startPosition.set(tempFantasma.position);
                            this.fantasmas.put(Fantasma.TIPO_FANTASMA.AMARILLO, tempFantasma);
                            break;

                        // Pacman
                        case '@':
                            pacman = new Pacman();
                            pacman.position.set(x, y);
                            pacman.startPosition.set(pacman.position);
                            break;

                        default:
                            Gdx.app.debug(TAG, "Caracter desconocido: '" + baldosa + "'");
                            break;
                    }
                }
            }

        }
        catch(FileNotFoundException ex){
            Gdx.app.debug(TAG, "No se puede abrir el archivo de nivel en '" + path +"'");
        }
        catch(IOException ex){
            Gdx.app.debug(TAG, "No se puede leer el archivo de nivel en '" + path +"'");
        }
    }

    private Array<Fruta.TIPO_FRUTA> ordenarFrutas(){
        Array<Fruta.TIPO_FRUTA> array = new Array<Fruta.TIPO_FRUTA>();

        array.add(Fruta.TIPO_FRUTA.CEREZA);
        array.add(Fruta.TIPO_FRUTA.FRESA);
        array.add(Fruta.TIPO_FRUTA.CEREZA);

        return array;
    }

    public void render(SpriteBatch batch){
        // Renderizar Muros del escenario
        for (Muro muro : this.muros){
            muro.render(batch);
        }

        // Renderizar cocos
        for (Coco coco : this.cocos){
            coco.render(batch);
        }

        // Renderizar fruta
        if (fruta != null){
            fruta.render(batch);
        }

        // Renderizar fantasmas
        for (Fantasma fantasma : this.fantasmas.values()){
            fantasma.render(batch);
        }

        // Renderizar pacman
        this.pacman.render(batch);
    }

    public void update(float deltaTime){
        pacman.update(deltaTime, this.atajo1, this.atajo2);

        for (Fantasma fantasma : this.fantasmas.values()){
            fantasma.update(deltaTime, this.pacman, this.posicionSalida);
        }

        if (cocosComidos == numCocos / 4){
            try {
                fruta = new Fruta(ordenFrutas.get(frutaActual));
            }catch (IndexOutOfBoundsException ex){
                Gdx.app.debug(TAG, "Se intenta cargar una fruta para un nivel inexistente - indice:" +
                        frutaActual);
                fruta = new Fruta(ordenFrutas.get(0)); // Se usa la cereza por defecto.
            }finally{
                fruta.position.set(posFruta);
            }
        }

        if (cocosComidos == 25){ // nivelGanado

        }
    }

    //Auxiliares
    public void encarcelarTodosFantasmas(Random rand){
        for (Fantasma fantasma : fantasmas.values()){
            float duracion = (rand.nextFloat() *4.0f) +1.0f;
            fantasma.encarcelar(duracion);
            Gdx.app.debug(TAG, "Fantasma: '" + fantasma.tipo.toString() + "' encarcelado: " + duracion + " segundos.");
        }
    }

}
