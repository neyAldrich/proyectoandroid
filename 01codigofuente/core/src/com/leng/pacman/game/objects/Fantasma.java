package com.leng.pacman.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.leng.pacman.game.Assets;

import java.util.Random;
import com.leng.pacman.game.objects.Pacman;

import com.badlogic.gdx.math.Vector2;
/**
 * Created by pc on 31/07/2015.
 */
public class Fantasma extends Agente{

    public enum TIPO_FANTASMA {ROJO, CELESTE, AMARILLO, ROSA}

    private TextureRegion regNormal;
    private TextureRegion regRender;
    private TextureRegion regVulnerable;
    public TIPO_FANTASMA tipo;

    private float velocidad;
    private int fpsCounter;
    private boolean enPrision;
    private float tiempoRestantePrision;

    public boolean esVulnerable = false;
    public boolean seguir = false;


    Random generador = new Random();
    int valor;

    public Fantasma(TIPO_FANTASMA tipo){
        this.tipo = tipo;
        init();
    }

    public void init(){

        esVulnerable = false;
        dimension.set(1.0f, 1.0f);
        velocidad = 5.0f;
        setTexture();
        bounds.set(0, 0, dimension.x, dimension.y);

        regRender = regNormal;
        regVulnerable = Assets.instance.fantasma.vulnerable;
        enPrision = true;
    }

    public void update (float deltaTime, Pacman pacman, Vector2 posSalida) {

        // PRISION

        if(enPrision){
            modificarTiempoPrison(-deltaTime, posSalida);
        }

        // ----

        if (pacman.hasPacmanPowerup()){
            if (!this.esVulnerable) this.esVulnerable = true;
        }else{
            if (this.esVulnerable) this.esVulnerable = false;
        }

        swapVulnerableTexture();

        if (this.seguir) {
            buscarPacman(deltaTime, pacman);
        }else{
            movAletorio(deltaTime);
        }
    }


    @Override
    public void render(SpriteBatch batch) {
        TextureRegion reg = null;

        reg = regRender;

        batch.draw(reg.getTexture(), position.x, position.y,
                origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y,
                rotation, reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(), false, false);
    }

    public int getScore(){
        return 250;
    }

    public void swapVulnerableTexture(){
        this.regRender = (esVulnerable) ? this.regVulnerable : this.regNormal;

    }

    public void setTexture(){
        switch (this.tipo){
            case ROJO:
                regNormal = Assets.instance.fantasma.rojo;
                seguir = true;
                this.tiempoRestantePrision = 2.0f;
                break;
            case CELESTE:
                regNormal = Assets.instance.fantasma.celeste;
                seguir = true;
                this.tiempoRestantePrision = 3.0f;
                break;
            case AMARILLO:
                regNormal = Assets.instance.fantasma.amarillo;
                this.tiempoRestantePrision = 4.0f;
                break;
            default:
                regNormal = Assets.instance.fantasma.rosa;
                this.tiempoRestantePrision = 5.0f;
        }
    }

    public DIRECTION generarDireccion(){
        valor =  generador.nextInt(4); // 0 Incluido - 4 excluido
        DIRECTION direccion = null;

        if (DIRECTION.DOWN.ordinal() == valor)
            direccion = DIRECTION.DOWN;
        else if(DIRECTION.UP.ordinal() == valor)
            direccion = DIRECTION.UP;
        else if(DIRECTION.LEFT.ordinal() == valor)
            direccion = DIRECTION.LEFT;
        else
            direccion = DIRECTION.RIGHT;

        return direccion;
    }

    public void setMovDirection(DIRECTION dir) {
        switch (dir) {
            case UP:
                direction.set(0.0f, 1.0f);
                break;
            case DOWN:
                direction.set(0.0f, -1.0f);
                break;
            case LEFT:
                direction.set(-1.0f, 0.0f);
                break;
            case RIGHT:
                direction.set(1.0f, 0.0f);
                break;
        }
    }

    public void movAletorio(float deltaTime){
        fpsCounter+=1;
        if (fpsCounter%15 == 0)
            setMovDirection(generarDireccion());

        velocity.x = direction.x * velocidad * deltaTime;
        velocity.y = direction.y * velocidad * deltaTime;

        position.x += velocity.x;
        position.y += velocity.y;
    }

    public void buscarPacman(float deltaTime, Pacman pacman){

        direction.set(position);

        direction.sub(pacman.position);

        if (!esVulnerable){
            direction.x = -direction.x;
            direction.y = -direction.y;
        }

        direction.nor();

        velocity.x = direction.x * velocidad * deltaTime;
        velocity.y = direction.y * velocidad * deltaTime;

        position.x += velocity.x;
        position.y += velocity.y;

    }

    public void encarcelar(float duracion){
        this.enPrision = true;
        this.tiempoRestantePrision = duracion;
        this.position.set(this.startPosition);
    }

    public void modificarTiempoPrison(float deltaTime, Vector2 posSalida){
        this.tiempoRestantePrision += deltaTime;

        if (tiempoRestantePrision <= 0){
            tiempoRestantePrision = 0;
            this.enPrision = false;
            this.position.set(posSalida);
        }
    }
}
