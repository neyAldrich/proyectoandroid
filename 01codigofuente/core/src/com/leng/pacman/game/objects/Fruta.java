package com.leng.pacman.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.leng.pacman.game.Assets;

/**
 * Created by pc on 31/07/2015.
 */
public class Fruta extends Agente {

    private static final String TAG = Fruta.class.getName();

    public enum TIPO_FRUTA {CEREZA, FRESA}

    private TextureRegion region;
    public TIPO_FRUTA tipo;

    public boolean collected;

    public Fruta(TIPO_FRUTA tipo){
        this.tipo = tipo;
        init();
    }

    public void init(){
        collected = false;
        dimension.set(1.0f, 1.0f);
        switch (this.tipo){
            case CEREZA:
                region = Assets.instance.frutas.cereza;
                break;
            case FRESA:
                region = Assets.instance.frutas.fresa;
                break;
            default:
                break;
        }
        bounds.set(0, 0, dimension.x, dimension.y);
    }

    @Override
    public void render(SpriteBatch batch) {
        if (collected) return;

        TextureRegion reg = null;
        reg = region;
        batch.draw(reg.getTexture(), position.x, position.y,
                origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y,
                rotation, reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(), false, false);

    }

    public int getScore() {
        int score = 0;
        switch (tipo) {
            case CEREZA:
                score = 100;
                break;
            case FRESA:
                score = 300;
                break;
            default:
                Gdx.app.debug(TAG, "Error en el metodo getScore");
        }
        return score;
    }
}