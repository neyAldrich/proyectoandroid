package com.leng.pacman.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.leng.pacman.game.Assets;

/**
 * Created by pc on 31/07/2015.
 */
public class Coco extends AbstractGameObject {

    public enum TIPO_COCO {NORMAL, SUPER}

    private TextureRegion reg;
    private TIPO_COCO tipo;

    public boolean collected;

    public Coco(TIPO_COCO tipo){
        this.tipo = tipo;
        init();
    }

    public void init(){

        dimension.set(1.0f, 1.0f);
        collected = false;
        switch (this.tipo){
            case NORMAL:
                reg = Assets.instance.coco.coco;
                break;
            case SUPER:
                reg = Assets.instance.coco.superCoco;
                break;
            default:
                break;
        }

        bounds.set(0, 0, dimension.x, dimension.y);

    }

    @Override
    public void render(SpriteBatch batch) {
        if (collected) return;

        batch.draw(reg.getTexture(), position.x, position.y,
                origin.x, origin.y, dimension.x, dimension.y, scale.x, scale.y,
                rotation, reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(), false, false);
    }

    public int getScore(){
        return (tipo == TIPO_COCO.NORMAL) ? 50 : 200;
    }

    public TIPO_COCO getTipo(){
        return this.tipo;
    }
}
