package com.leng.pacman.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by pc on 30/07/2015.
 */
public abstract class Agente extends AbstractGameObject {

    public Vector2 velocity;
    public Vector2 direction;

    public Vector2 startPosition;

    public enum DIRECTION { LEFT, RIGHT, UP, DOWN }

    public Agente(){

        super();
        velocity = new Vector2(0.0f, 0.0f);
        direction = new Vector2(0.0f, 0.0f);
        origin.set(dimension.x / 2.0f, dimension.y / 2.0f);
        startPosition = new Vector2();
    }

    public void update (float deltaTime){
        // Mover a la siguiente posicion
        //position.x += velocity.x * deltaTime;
        //position.y += velocity.y * deltaTime;
    }

    public abstract void render(SpriteBatch batch);

}