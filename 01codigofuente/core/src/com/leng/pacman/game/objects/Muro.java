package com.leng.pacman.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.leng.pacman.game.Assets;

/**
 * Created by Neycker on 01/08/2015.
 */
public class Muro extends AbstractGameObject {

    public enum TIPO_MURO {GLASS, RED_BRICKS, LIGHT_BRICKS, PUERTA_PRISION, SIMPLE }

    private TextureRegion muroRegion = new TextureRegion();
    private TIPO_MURO tipo;
    private Color tint;

    public Muro(TIPO_MURO tipo){
        this.tipo = tipo;
        this.tint = null;
        init();
    }

    public void init(){
        dimension.set(1.0f, 1.0f);
        origin.set(dimension.x / 2.0f, dimension.y / 2.0f);

        switch (tipo) {
            case GLASS:
                muroRegion = Assets.instance.decoracion.muro_glass;
                break;
            case RED_BRICKS:
                muroRegion = Assets.instance.decoracion.muro_red_bricks;
                break;
            case LIGHT_BRICKS:
                muroRegion = Assets.instance.decoracion.muro_light_bricks;
                break;
            case SIMPLE:
                muroRegion = Assets.instance.decoracion.muro_simple;
                break;
            case PUERTA_PRISION:
                muroRegion = Assets.instance.decoracion.puerta_prision;
                break;
        }

        bounds.set(0, 0, dimension.x, dimension.y);
    }

    @Override
    public void render(SpriteBatch batch) {
        TextureRegion reg = null;
        reg = muroRegion;

        if(tint != null) batch.setColor(tint);

        batch.draw(reg.getTexture(), position.x, position.y, origin.x, origin.y, dimension.x, dimension.y,
                scale.x, scale.y, rotation, reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(), false, false);

        if(tint != null) batch.setColor(Color.WHITE);
    }

    public void setTint(Color color){
        this.tint = color;
    }

}
