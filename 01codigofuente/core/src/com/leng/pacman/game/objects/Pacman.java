package com.leng.pacman.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.leng.pacman.game.Assets;
import com.leng.pacman.util.Constantes;
import com.leng.pacman.util.AudioManager;
import java.awt.*;
import java.util.Vector;

/**
 * Created by pc on 31/07/2015.
 */
public class Pacman extends Agente{

    public static final String TAG = Pacman.class.getName();

    public float maxVelocity;    //< Metros por segundo

    private TextureRegion reg;
    private TextureRegion regAnimation1;
    private TextureRegion regAnimation2;
    public DIRECTION viewDirection;
    public boolean hasPacmanPowerup;
    public float timeLeftPacmanPowerup;

    public float timePassed = 0.0f;

    public Pacman(){init();}

    public void init () {
        maxVelocity = Constantes.PACMAN_VELOCITY;

        dimension.set(1, 1);
        regAnimation1 = Assets.instance.pacman.pacman1;
        regAnimation2 = Assets.instance.pacman.pacman2;

        reg = regAnimation1;
        // Bounding box for collision detection
        bounds.set(0, 0, dimension.x, dimension.y);
        // Direccion de movimiento
        viewDirection = DIRECTION.LEFT;

        // Power-ups
        hasPacmanPowerup = false;
        timeLeftPacmanPowerup = 0;

    };

    public void setPacmanPowerup (boolean pickedUp) {
        hasPacmanPowerup = pickedUp;
        if (pickedUp) {
            timeLeftPacmanPowerup = Constantes.ITEM_COCO_POWERUP_DURATION;
        }
    }

    public boolean hasPacmanPowerup () {
        return hasPacmanPowerup && timeLeftPacmanPowerup > 0;
    }

    public void update (float deltaTime, Vector2 atajo1, Vector2 atajo2) {

        // Animación
        updateAnimation(deltaTime);

        // Atajos : Importante que se realice antes que la actualizacion de posicion:
        // ya que a diferencia de los proyectos en python y c++ no realizamos la colision
        // con el escenario dentro de este metodo.
        if (this.position.equals(atajo1)){
            this.position.set(atajo2);
        }else if(this.position.x == atajo2.x && this.position.y == atajo2.y){
            this.position.set(atajo1);
        }
        //

        velocity.x = direction.x * maxVelocity * deltaTime;
        velocity.y = direction.y * maxVelocity * deltaTime;

        position.x += velocity.x;
        position.y += velocity.y;

        /*
        if (timeLeftPacmanPowerup > 0) {
            timeLeftPacmanPowerup -= deltaTime;
            if (timeLeftPacmanPowerup < 0) {
                // disable power-up
                timeLeftPacmanPowerup = 0;
                setPacmanPowerup(false);
            }
        }
        */

        // PODERES

        if (timeLeftPacmanPowerup > 0){
            timeLeftPacmanPowerup -= deltaTime;
            if (timeLeftPacmanPowerup < 0){
                // disable power-up
                timeLeftPacmanPowerup = 0;
                setPacmanPowerup(false);
                Assets.instance.music.song01.stop();
            }
        }
    }

    @Override
    public void render(SpriteBatch batch) {

        batch.draw(reg.getTexture(), position.x, position.y, origin.x,
                origin.y, dimension.x, dimension.y, scale.x, scale.y, rotation,
                reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
                reg.getRegionHeight(), false,
                false);
    }

    public void setViewDirection(DIRECTION dir){
        switch (dir){
            case LEFT:
                rotation = 180;
                break;
            case UP:
                rotation = 90;
                break;
            case DOWN:
                rotation = -90;
                break;
            case RIGHT:
                rotation = 0;
                break;
        }
    }

    public void setMovDirection(DIRECTION dir){
        switch (dir){
            case UP:
                direction.set(0.0f, 1.0f);
                break;
            case DOWN:
                direction.set(0.0f, -1.0f);
                break;
            case LEFT:
                direction.set(-1.0f, 0.0f);
                break;
            case RIGHT:
                direction.set(1.0f, 0.0f);
                break;
        }
        setViewDirection(dir);
    }

    public void updateAnimation(float deltaTime){
        timePassed += deltaTime;

        if (timePassed > 0.4f){ // 0.4 segundos
            timePassed = 0.0f;

            reg = (reg == regAnimation1) ? regAnimation2 : regAnimation1;
        }
    }
}
