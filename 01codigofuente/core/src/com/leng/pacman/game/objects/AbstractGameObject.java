package com.leng.pacman.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Neycker on 31/07/2015.
 */
public abstract class AbstractGameObject {

    public Vector2 position;
    public Vector2 dimension;
    public Vector2 origin;
    public Vector2 scale;
    public float rotation;

    public Rectangle bounds;

    public AbstractGameObject(){
        position = new Vector2();
        dimension = new Vector2(1, 1);
        origin = new Vector2();
        scale = new Vector2(1, 1);
        rotation = 0;
        bounds = new Rectangle();
    }

    public void update (float deltaTime){}

    public abstract void render(SpriteBatch batch);

    public Vector2 getCenterPos(){
        return new Vector2(position.x + (dimension.x / 2.0f), position.y + (dimension.y / 2.0f));
    }
}
